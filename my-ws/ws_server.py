# pip install websockets
# WS server example
import asyncio
import websockets


# It reads a name from the client, sends a greeting,
# and closes the connection.
async def hello(websocket, path):
    name = await websocket.recv()
    print(f"< {name}")

    greeting = f"Hello {name}!"

    await websocket.send(greeting)
    print(f"> {greeting}")

start_server = websockets.serve(hello, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
# asyncio.run(start_server) NG
